# BrowseEmAll.Cef#

BrowseEmAll.Cef is a .NET Winforms control for embedding the [Chrome browser](https://www.google.com/chrome/) or more precisely for embedding the rendering engine (Blink) and JavaScript engine (V8).

This project supports embedding and controlling of the Chrome browser into any .NET application.

## Getting started ##

To integrate BrowseEmAll.Cef into your application simply follow the below steps:

* Download the latest build of BrowseEmAll.Cef at the Downloads section
* Download the necessary CEF build from [CefBuilds.com](http://www.cefbuilds.com)
* Add a reference for BrowseEmAll.Cef.dll and BrowseEmAll.Cef.Winforms.dll to your .NET project
* Extract the Cef build into the output folder of your application. The Resources and Release folder content is used.
* Initialize Chrome at the start of your application with 
```
#!c#
var mainArgs = CefMainArgs(args);
var app = new DemoApp();
var settings = new CefSettings();
CefRuntime.Load();
CefRuntime.ExecuteProcess(mainArgs, app, IntPtr.Zero);
CefRuntime.Initialze(mainArgs, settings, app, IntPtr.Zero);
```
* Create a new CefWebBrowser and add it to your UI
```
#!c#
var browser = new CefWebBrowser();
// Add browser to your UI somehow
// panel.Controls.Add(browser);
```

You can find a full example in BrowseEmAll.Cef.Client in this repository.

## Differences to CefGlue ##

This project is based on the work done for [CefGlue](http://xilium.bitbucket.org/cefglue/). This project aims to follow the latest releases of Chrome more closely.

## Important Methods / Events ##

The most used methods are:

* LoadUrl(string url)
* GoBack()
* GoForward()
* StopLoad()
* Reload()

The most used events are:

* LoadStarted
* LoadEnd
* StatusMessage
* TitleChanged

## Licensing ##

This project is licensed under MIT License with portions of code licensed under New BSD License.