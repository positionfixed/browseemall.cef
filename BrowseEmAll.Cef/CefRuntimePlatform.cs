﻿namespace BrowseEmAll.Cef
{
    using System;

    public enum CefRuntimePlatform
    {
        Windows,
        Linux,
        MacOSX,
    }
}
