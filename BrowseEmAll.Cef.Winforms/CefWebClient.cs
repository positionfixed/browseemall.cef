﻿namespace BrowseEmAll.Cef.Winforms
{
    using System;
    using System.Collections.Generic;
    using BrowseEmAll.Cef;

    public class CefWebClient : CefClient
    {
        private readonly CefWebBrowser _core;
        private readonly CefWebLifeSpanHandler _lifeSpanHandler;
        private readonly CefWebDisplayHandler _displayHandler;
        private readonly CefWebLoadHandler _loadHandler;
        private readonly CefWebRequestHandler _requestHandler;
        private readonly CefWebClientDownloadHandler _downloadHandler;
        private readonly CefWebJSDialogHandler _jsHandler;
        private readonly CefWebKeyboardHandler _keyHandler;
        private readonly CefWebContextMenuHandler _contextMenuHandler;
        private readonly CefWebRenderHandler _rendererHandler;

        public CefWebClient(CefWebBrowser core)
        {
            _core = core;
            _lifeSpanHandler = new CefWebLifeSpanHandler(_core);
            _displayHandler = new CefWebDisplayHandler(_core);
            _loadHandler = new CefWebLoadHandler(_core);
            _requestHandler = new CefWebRequestHandler(_core);
            _downloadHandler = new CefWebClientDownloadHandler(_core);
            _jsHandler = new CefWebJSDialogHandler(_core);
            _keyHandler = new CefWebKeyboardHandler(_core);
            _contextMenuHandler = new CefWebContextMenuHandler(_core);
            _rendererHandler = new CefWebRenderHandler(_core);
        }

        protected CefWebBrowser Core { get { return _core; } }

        protected override CefRenderHandler GetRenderHandler()
        {
            return _rendererHandler;
        }

        protected override CefLifeSpanHandler GetLifeSpanHandler()
        {
            return _lifeSpanHandler;
        }

        protected override CefDisplayHandler GetDisplayHandler()
        {
            return _displayHandler;
        }

        protected override CefDownloadHandler GetDownloadHandler()
        {
            return _downloadHandler;
        }

        protected override CefLoadHandler GetLoadHandler()
        {
            return _loadHandler;
        }

        protected override CefRequestHandler GetRequestHandler()
        {
            return _requestHandler;
        }

        protected override CefJSDialogHandler GetJSDialogHandler()
        {
            return _jsHandler;
        }

        protected override CefKeyboardHandler GetKeyboardHandler()
        {
            return _keyHandler;
        }

        protected override CefContextMenuHandler GetContextMenuHandler()
        {
            return _contextMenuHandler;
        }

        protected override bool OnProcessMessageReceived(CefBrowser browser, CefProcessId sourceProcess, CefProcessMessage message)
        {
            if (message.Name.Equals("ResultJSMessage"))
            {
                int type = message.Arguments.GetInt(0);
                bool returnBool = false;
                double returnDouble = 0;
                string returnString = string.Empty;

                switch (type)
                {
                    case 0:
                        returnBool = message.Arguments.GetBool(1);
                        break;
                    case 1:
                        returnDouble = message.Arguments.GetDouble(1);
                        break;
                    case 2:
                        returnString = message.Arguments.GetString(1);
                        break;

                }
                _core.OnJavaScriptReturn(type, returnBool, returnDouble, returnString, message.Arguments.GetString(2));
            }
            else if (message.Name.Equals("ActionPerformedMessage"))
            {
                _core.OnActionPerformed(message.Arguments.GetString(0));
            }
            else if (message.Name.Equals("ScreenshotRequestMessage"))
            {
                _core.OnScreenshotRequest(message.Arguments.GetString(0));
            }

            return false;
        }
    }
}
