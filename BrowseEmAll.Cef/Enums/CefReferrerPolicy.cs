﻿//
// This file manually written from cef/include/internal/cef_types.h.
// C API name: cef_color_model_t.
//
namespace BrowseEmAll.Cef
{
    /// <summary>
    /// referer policy mode values.
    /// </summary>
    public enum CefReferrerPolicy
    {
        // Always send the complete Referrer value.
        REFERRER_POLICY_ALWAYS,

        // Use the default policy. This is REFERRER_POLICY_ORIGIN_WHEN_CROSS_ORIGIN
        // when the `--reduced-referrer-granularity` command-line flag is specified
        // and REFERRER_POLICY_NO_REFERRER_WHEN_DOWNGRADE otherwise.
        REFERRER_POLICY_DEFAULT,

        // When navigating from HTTPS to HTTP do not send the Referrer value.
        // Otherwise, send the complete Referrer value.
        REFERRER_POLICY_NO_REFERRER_WHEN_DOWNGRADE,

        // Never send the Referrer value.
        REFERRER_POLICY_NEVER,

        // Only send the origin component of the Referrer value.
        REFERRER_POLICY_ORIGIN,

        // When navigating cross-origin only send the origin component of the Referrer
        // value. Otherwise, send the complete Referrer value.
        REFERRER_POLICY_ORIGIN_WHEN_CROSS_ORIGIN,
    }
}
