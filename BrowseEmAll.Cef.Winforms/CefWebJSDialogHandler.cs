﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal sealed class CefWebJSDialogHandler:CefJSDialogHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebJSDialogHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override bool OnJSDialog(CefBrowser browser, string originUrl, CefJSDialogType dialogType, string message_text, string default_prompt_text, CefJSDialogCallback callback, out bool suppress_message)
        {
            suppress_message = _core.Silent;
            return false;
        }

        protected override bool OnBeforeUnloadDialog(CefBrowser browser, string messageText, bool isReload, CefJSDialogCallback callback)
        {
            return _core.Silent;
        }

        protected override void OnResetDialogState(CefBrowser browser)
        {

        }

        protected override void OnDialogClosed(CefBrowser browser)
        {
            
        }
    }
}
