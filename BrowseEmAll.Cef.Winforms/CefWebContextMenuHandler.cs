﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal class CefWebContextMenuHandler : CefContextMenuHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebContextMenuHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override void OnBeforeContextMenu(CefBrowser browser, CefFrame frame, CefContextMenuParams state, CefMenuModel model)
        {
            _core.OnContextMenu(state.LinkUrl);
            model.Clear();
        }
    }
}
