﻿namespace BrowseEmAll.Cef.Winforms
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public sealed class StatusTextEventArgs : EventArgs
    {
        private readonly string _value;

        public StatusTextEventArgs(string value)
        {
            _value = value;
        }

        public string Value { get { return _value; } }
    }
}
