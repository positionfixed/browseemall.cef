﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrowseEmAll.Cef.Winforms
{
    internal sealed class CefWebRequestionHandler : CefRequestHandler
    {
        private readonly CefWebBrowser _core;

        public CefWebRequestionHandler(CefWebBrowser core)
        {
            _core = core;
        }

        protected override bool GetAuthCredentials(CefBrowser browser, CefFrame frame, bool isProxy, string host, int port, string realm, string scheme, CefAuthCallback callback)
        {
            string username = string.Empty;
            string password = string.Empty;

            if (_core.GetAuthCredentials(out username, out password))
            {
                callback.Continue(username, password);
                return true;
            }

            callback.Cancel();
            return false;
        }

    }
}
